package devcamp.shop.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import devcamp.shop.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Integer> {
        @Query(value = """
                        FROM COrder r
                        WHERE (:customer IS NULL OR :customer = r.customer.id)
                        AND (:startDate IS NULL OR :endDate IS NULL OR r.orderDate BETWEEN :startDate and :endDate)
                        AND (:keyword IS NULL OR LOWER(r.customer.username) LIKE CONCAT('%', LOWER(:keyword), '%'))
                                """)
        public Page<COrder> findOrderByRequest(
                        Optional<Integer> customer,
                        Optional<String> keyword,
                        Date startDate,
                        Date endDate,
                        Pageable pageable);

        @Query(value = """
                        SELECT DATE(o.order_date) as date, DAYNAME(o.order_date) as dayOfWeek, DAYOFMONTH(o.order_date) as dayOfMonth, WEEK(o.order_date, 3) as week, MONTHNAME(o.order_date) as month, SUM(od.quantity * od.sell_price) as revenue
                        FROM orders o
                        JOIN order_detail od ON o.id = od.order_id
                        WHERE o.order_date BETWEEN :startDate and :endDate
                        GROUP BY
                                CASE
                           	        WHEN :type = 1 THEN DATE(o.order_date)
                           	        WHEN :type = 2 THEN DAYNAME(o.order_date)
                                        WHEN :type = 3 THEN DAYOFMONTH(o.order_date)
                                        WHEN :type = 4 THEN WEEK(o.order_date, 3)
                                        WHEN :type = 5 THEN MONTHNAME(o.order_date)
                                END
                        ORDER BY o.order_date ASC
                                """, nativeQuery = true)
        List<COrder.Report> reportOrders(Date startDate, Date endDate, Integer type);

}
