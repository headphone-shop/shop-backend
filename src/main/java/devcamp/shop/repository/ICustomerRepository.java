package devcamp.shop.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import devcamp.shop.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Integer> {
        public CCustomer findByUsername(String username);

        @Query(value = """
                        SELECT *, SUM(od.quantity * od.sell_price)
                        FROM customer c
                        JOIN user u ON u.id = c.id
                        LEFT JOIN orders o ON o.customer_id = c.id
                        LEFT JOIN order_detail od ON o.id = od.order_id
                        GROUP By c.id
                        HAVING (:minPayment IS NULL OR :maxPayment IS NULL OR SUM(od.quantity * od.sell_price) BETWEEN :minPayment AND :maxPayment)
                        AND (:minOrder IS NULL OR :maxOrder IS NULL OR COUNT(DISTINCT o.id) BETWEEN :minOrder AND :maxOrder )
                        """, nativeQuery = true)
        Page<CCustomer> reportCustomers(Integer minPayment, Integer maxPayment, Integer minOrder, Integer maxOrder,
                        Pageable pageable);
}
