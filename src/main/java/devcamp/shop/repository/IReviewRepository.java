package devcamp.shop.repository;

import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import devcamp.shop.model.CReview;

public interface IReviewRepository extends JpaRepository<CReview, Integer> {

        @Query(value = """
                        FROM CReview r
                        WHERE (:product IS NULL OR r.product.id = :product)
                        """)
        public Set<CReview> findReviewByProductId(
                        Optional<Integer> product);

        @Query(value = """
                        FROM CReview r
                        WHERE (:product IS NULL OR r.product.id = :product)
                            AND (:customer IS NULL OR r.customer.id = :customer)
                            AND (:order IS NULL OR r.order.id = :order)
                            AND (:rate IS NULL OR r.rate = :rate)
                            AND (:haveComment IS NULL OR r.reviewText != '')
                            AND (:keyword IS NULL OR LOWER(r.reviewText) LIKE CONCAT('%', LOWER(:keyword), '%'))
                        """)
        public Page<CReview> findReviewByRequest(
                        Optional<Integer> product,
                        Optional<Integer> rate,
                        Optional<String> customer,
                        Optional<Integer> order,
                        Optional<String> haveComment,
                        Optional<String> keyword,
                        Pageable pageable);

        @Query(value = """
                        FROM CReview r
                        WHERE (:product IS NULL OR r.product = :product)
                        AND(r.reviewText != '')
                        """)
        public Page<CReview> findReviewByReviewText(Optional<Integer> product, Pageable pageable);

}
