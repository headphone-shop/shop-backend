package devcamp.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.shop.model.CProductLine;

public interface IProductLineRepository extends JpaRepository<CProductLine, Integer> {

}
