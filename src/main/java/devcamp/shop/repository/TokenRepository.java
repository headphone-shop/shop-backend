package devcamp.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.shop.login.Token;

public interface TokenRepository extends JpaRepository<Token, Long> {

    Token findByToken(String token);

    void deleteByToken(String token);

}
