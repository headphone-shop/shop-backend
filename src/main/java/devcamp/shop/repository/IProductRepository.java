package devcamp.shop.repository;

import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import devcamp.shop.model.CProduct;

public interface IProductRepository extends JpaRepository<CProduct, Integer> {
        // @Query(value = """
        // FROM CProduct r
        // WHERE (COALESCE(:brand) IS NULL OR r.brand.id IN (:brand))
        // AND (:statusPrice IS NULL OR :statusPrice = r.statusPrice)
        // AND (:priceMin IS NULL OR :priceMax IS NULL OR r.sellPrice BETWEEN :priceMin
        // AND :priceMax)
        // AND (:category IS NULL OR :category = r.category.id)
        // AND (:color IS NULL OR :color = r.color.id)
        // AND (:rate IS NULL OR :rate = r.rate)
        // AND (:keyword IS NULL OR LOWER(r.productName) LIKE CONCAT('%',
        // LOWER(:keyword), '%')
        // """)
        // public Page<CProduct> findProductByRequest(
        // Optional<Integer> brand,
        // Optional<Integer> statusPrice,
        // Optional<Double> priceMin, Optional<Double> priceMax,
        // Optional<Integer> category,
        // Optional<Integer> color,
        // Optional<Integer> rate,
        // Optional<String> keyword,
        // Pageable pageable);

        // }
        @Query(value = """
                        FROM CProduct r
                        WHERE (COALESCE(:brand) IS NULL OR r.brand.id IN (:brand))
                        AND (:priceMin IS NULL OR :priceMax IS NULL OR r.sellPrice BETWEEN :priceMin AND :priceMax)
                        AND (:category IS NULL OR :category = r.category.id)
                        AND (:color IS NULL OR :color = r.color.id)
                        AND (:keyword IS NULL OR LOWER(r.productName) LIKE CONCAT('%', LOWER(:keyword), '%'))
                        """)
        public Page<CProduct> findProductByRequest(
                        Optional<Integer> brand,
                        Optional<Double> priceMin,
                        Optional<Double> priceMax,
                        Optional<Integer> category,
                        Optional<Integer> color,
                        Optional<String> keyword,
                        Pageable pageable);

}
