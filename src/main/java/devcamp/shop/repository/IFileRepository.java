package devcamp.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.shop.model.CFile;

public interface IFileRepository extends JpaRepository<CFile, Integer> {

}
