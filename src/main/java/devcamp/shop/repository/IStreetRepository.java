package devcamp.shop.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import devcamp.shop.model.CStreet;

public interface IStreetRepository extends JpaRepository<CStreet, Integer> {
    List<CStreet> findByDistrictId(int id);

    @Query(value = """
            FROM CStreet s
            WHERE (:province IS NULL OR :province = s.district.province.id)
                AND (:district IS NULL OR :district = s.district.id)
                AND (:keyword IS NULL OR LOWER(s.streetName) LIKE CONCAT('%', LOWER(:keyword), '%') OR LOWER(s.district.districtName) LIKE CONCAT('%', LOWER(:keyword), '%') OR LOWER(s.district.province.provinceName) LIKE CONCAT('%', LOWER(:keyword), '%'))
                    """)
    public Page<CStreet> findWithPaging(
            Optional<Integer> province,
            Optional<Integer> district,
            Optional<String> keyword,
            Pageable pageable);

}
