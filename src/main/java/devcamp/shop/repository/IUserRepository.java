package devcamp.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.shop.model.CUser;

public interface IUserRepository extends JpaRepository<CUser, Integer> {
    CUser findByUsername(String username);

}
