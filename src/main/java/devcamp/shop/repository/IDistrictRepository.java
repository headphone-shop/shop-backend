package devcamp.shop.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import devcamp.shop.model.CDistrict;

public interface IDistrictRepository extends JpaRepository<CDistrict, Integer> {
    List<CDistrict> findByProvinceId(int id);

    @Query(value = """
            FROM CDistrict d
            WHERE (:province IS NULL OR :province = d.province.id)
                AND (:keyword IS NULL OR LOWER(d.districtName) LIKE CONCAT('%', LOWER(:keyword), '%') OR LOWER(d.province.provinceName) LIKE CONCAT('%', LOWER(:keyword), '%'))
                    """)
    public Page<CDistrict> findWithPaging(
            Optional<Integer> province,
            Optional<String> keyword,
            Pageable pageable);
}
