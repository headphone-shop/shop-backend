package devcamp.shop.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import devcamp.shop.model.CStreet;
import devcamp.shop.repository.IStreetRepository;
import devcamp.shop.service.StreetService;

@CrossOrigin
@RestController
@RequestMapping("/streets")
public class StreetController {
    @Autowired
    StreetService streetService;

    @Autowired
    IStreetRepository streetRepository;

    @GetMapping("/{id}")
    public ResponseEntity<CStreet> getStreetByStreetId(
            @PathVariable("id") int streetId) {
        try {
            CStreet findStreet = streetService.getStreetById(streetId);
            return new ResponseEntity<>(findStreet, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<CStreet>> getStreets() {
        try {
            List<CStreet> lstStreet = streetService.getStreets();
            return new ResponseEntity<>(lstStreet, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/paging")
    public ResponseEntity<Object> findWithPaging(
            @RequestParam(value = "province", required = false) Optional<Integer> province,
            @RequestParam(value = "district", required = false) Optional<Integer> district,
            @RequestParam(value = "keyword", required = false) Optional<String> keyword,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "6") int size,
            @RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
            @RequestParam(value = "sortDirection", defaultValue = "DESC") Direction sortDirection) {
        try {
            return new ResponseEntity<>(streetRepository.findWithPaging(
                    province,
                    district,
                    keyword,
                    PageRequest.of(page, size, Sort.by(sortDirection, sortBy))), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> createStreet(@Valid @RequestBody CStreet cStreet) {
        try {
            CStreet savedStreet = streetService.createStreet(cStreet);
            return new ResponseEntity<>(savedStreet, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> updateStreet(@PathVariable("id") int id, @Valid @RequestBody CStreet cStreet) {
        try {
            CStreet savedStreet = streetService.updateStreet(id, cStreet);
            return new ResponseEntity<>(savedStreet, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteStreetById(@PathVariable int id) {
        try {
            streetService.deleteStreet(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
