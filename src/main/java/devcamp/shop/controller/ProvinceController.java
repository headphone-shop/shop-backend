package devcamp.shop.controller;

import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import devcamp.shop.model.CDistrict;
import devcamp.shop.model.CProvince;
import devcamp.shop.service.DistrictService;
import devcamp.shop.service.ProvinceService;

@CrossOrigin
@RestController
@RequestMapping("/provinces")
public class ProvinceController {
    @Autowired
    ProvinceService provinceService;

    @Autowired
    DistrictService districtService;

    @GetMapping("/")
    public ResponseEntity<List<CProvince>> getProvinces() {
        try {
            List<CProvince> lstProvince = provinceService.getProvinces();
            return new ResponseEntity<List<CProvince>>(lstProvince, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<CProvince> getProvinceById(@PathVariable("id") int id) {
        try {
            CProvince nProvince = provinceService.getProvinceById(id);
            return new ResponseEntity<>(nProvince, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}/districts")
    public ResponseEntity<List<CDistrict>> getDistrictsByProvinceId(@PathVariable("id") int id) {
        try {
            List<CDistrict> districts = districtService.findByProvinceId(id);
            return new ResponseEntity<>(districts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<CProvince> createProvince(@Valid @RequestBody CProvince nProvince) {
        try {
            CProvince saveProvince = provinceService.createProvince(nProvince);
            return new ResponseEntity<CProvince>(saveProvince, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<CProvince> updateProvince(@PathVariable("id") int id,
            @Valid @RequestBody CProvince nProvince) {
        try {
            CProvince saveProvince = provinceService.updateProvince(id, nProvince);
            return new ResponseEntity<CProvince>(saveProvince, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteProvince(@PathVariable("id") int id) {
        try {
            provinceService.deleteProvince(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
