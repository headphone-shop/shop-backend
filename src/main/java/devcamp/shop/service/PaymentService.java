package devcamp.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CPayment;
import devcamp.shop.repository.IPaymentRepository;

@Service
public class PaymentService {

    @Autowired
    IPaymentRepository paymentRepository;

    public List<CPayment> getPayments() {
        return paymentRepository.findAll();
    }

    public CPayment getPaymentById(int id) {
        return paymentRepository.findById(id).get();
    }

    public CPayment createPayment(CPayment nPayment) {
        CPayment savedPayment = paymentRepository.save(nPayment);
        return savedPayment;
    }

    public CPayment updatePayment(int id,
            CPayment nPayment) {
        CPayment oldPayment = getPaymentById(id);
        oldPayment.setAmount(nPayment.getAmount());
        oldPayment.setCustomer(nPayment.getCustomer());
        oldPayment.setOrder(nPayment.getOrder());
        oldPayment.setType(nPayment.getType());

        return paymentRepository.save(oldPayment);
    }

    public void deletePayment(int id) {
        CPayment oldPayment = getPaymentById(id);
        paymentRepository.deleteById(oldPayment.getId());
    }
}
