package devcamp.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CDiscount;
import devcamp.shop.repository.IDiscountRepository;

@Service
public class DiscountService {

    @Autowired
    IDiscountRepository discountRepository;

    public List<CDiscount> getDiscounts() {
        return discountRepository.findAll();
    }

    public CDiscount getDiscountById(int id) {
        return discountRepository.findById(id).get();
    }

    public CDiscount createDiscount(CDiscount nDiscount) {

        CDiscount savedDiscount = discountRepository.save(nDiscount);
        return savedDiscount;
    }

    public CDiscount updateDiscount(int id,
            CDiscount nDiscount) {
        CDiscount oldDiscount = getDiscountById(id);
        oldDiscount.setDiscountCode(nDiscount.getDiscountCode());
        oldDiscount.setDiscountDescription(nDiscount.getDiscountDescription());
        oldDiscount.setPercent(nDiscount.getPercent());

        return discountRepository.save(oldDiscount);
    }

    public void deleteDiscount(int id) {
        CDiscount oldDiscount = getDiscountById(id);
        discountRepository.deleteById(oldDiscount.getId());
    }
}
