package devcamp.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CProvince;
import devcamp.shop.repository.IDistrictRepository;
import devcamp.shop.repository.IProvinceRepository;
import devcamp.shop.repository.IWardRepository;

@Service
public class ProvinceService {

    @Autowired
    IProvinceRepository provinceRepository;

    @Autowired
    IDistrictRepository districtRepository;

    @Autowired
    IWardRepository wardRepository;

    public List<CProvince> getProvinces() {
        return provinceRepository.findAll();
    }

    public CProvince getProvinceById(int id) {
        return provinceRepository.findById(id).get();
    }

    public CProvince createProvince(CProvince nProvince) {
        return provinceRepository.save(nProvince);
    }

    public CProvince updateProvince(int id, CProvince nProvince) {
        CProvince oldProvince = getProvinceById(id);
        oldProvince.setProvinceName(nProvince.getProvinceName());
        oldProvince.setCode(nProvince.getCode());

        return provinceRepository.save(oldProvince);
    }

    public void deleteProvince(int id) {
        CProvince oldProvince = getProvinceById(id);
        provinceRepository.deleteById(oldProvince.getId());
    }
}
