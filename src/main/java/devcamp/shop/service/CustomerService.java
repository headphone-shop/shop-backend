package devcamp.shop.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CCustomer;
import devcamp.shop.model.CDistrict;
import devcamp.shop.model.CEmployee;
import devcamp.shop.model.CProvince;
import devcamp.shop.model.CStreet;
import devcamp.shop.model.CUser;
import devcamp.shop.model.CWard;
import devcamp.shop.repository.ICustomerRepository;
import devcamp.shop.repository.IDistrictRepository;
import devcamp.shop.repository.IProvinceRepository;
import devcamp.shop.repository.IStreetRepository;
import devcamp.shop.repository.IUserRepository;
import devcamp.shop.repository.IWardRepository;

@Service
public class CustomerService {
    @Autowired
    ICustomerRepository customerRepository;

    @Autowired
    IUserRepository userRepository;

    @Autowired
    IWardRepository wardRepository;

    @Autowired
    IProvinceRepository provinceRepository;

    @Autowired
    IDistrictRepository districtRepository;

    @Autowired
    IStreetRepository streetRepository;

    public List<CCustomer> getCustomers() {
        return customerRepository.findAll();
    }

    public CCustomer getCustomerById(Integer id) {
        return customerRepository.findById(id).get();
    }

    public CCustomer createCustomer(CCustomer nCustomer) {
        if (nCustomer.getProvince() != null) {
            CProvince province = provinceRepository.findById(nCustomer.getProvince().getId()).get();
            nCustomer.setProvince(province);
        }

        if (nCustomer.getDistrict() != null) {
            CDistrict district = districtRepository.findById(nCustomer.getDistrict().getId()).get();
            nCustomer.setDistrict(district);
        }

        if (nCustomer.getWard() != null) {
            CWard ward = wardRepository.findById(nCustomer.getWard().getId()).get();
            nCustomer.setWard(ward);
        }

        if (nCustomer.getStreet() != null) {
            CStreet street = streetRepository.findById(nCustomer.getStreet().getId()).get();
            nCustomer.setStreet(street);
        }

        return customerRepository.save(nCustomer);
    }

    public CCustomer updateCustomer(Integer id, CCustomer newInfo) {
        CCustomer oldCustomer = getCustomerById(id);

        oldCustomer.setEmail(newInfo.getEmail());
        oldCustomer.setFullname(newInfo.getFullname());
        oldCustomer.setPhone(newInfo.getPhone());

        if (newInfo.getProvince() != null) {
            CProvince province = provinceRepository.findById(newInfo.getProvince().getId()).get();
            oldCustomer.setProvince(province);
        } else {
            oldCustomer.setProvince(null);
        }

        if (newInfo.getDistrict() != null) {
            CDistrict district = districtRepository.findById(newInfo.getDistrict().getId()).get();
            oldCustomer.setDistrict(district);
        } else {
            oldCustomer.setDistrict(null);
        }

        if (newInfo.getWard() != null) {
            CWard ward = wardRepository.findById(newInfo.getWard().getId()).get();
            oldCustomer.setWard(ward);
        } else {
            oldCustomer.setWard(null);
        }

        if (newInfo.getStreet() != null) {
            CStreet street = streetRepository.findById(newInfo.getStreet().getId()).get();
            oldCustomer.setStreet(street);
        } else {
            oldCustomer.setStreet(null);
        }

        return customerRepository.saveAndFlush(oldCustomer);
    }

    public void deleteCustomer(Integer id) {
        CCustomer oldCustomer = getCustomerById(id);
        customerRepository.delete(oldCustomer);
    }

    public CCustomer findByUsername(String username) {
        return customerRepository.findByUsername(username);
    }
}
