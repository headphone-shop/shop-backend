package devcamp.shop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "product_line")
public class CProductLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "Type", columnDefinition = "ENUM('brand', 'color','category')")
    @Enumerated(EnumType.STRING)
    private SelectType type;

    @NotNull(message = "Chưa nhập tên")
    @Column(name = "name")
    private String name;

    public enum SelectType {
        brand, color, category
    }

    public CProductLine(int id, SelectType type, @NotNull(message = "Chưa nhập tên") String name) {
        this.id = id;
        this.type = type;
        this.name = name;
    }

    public CProductLine() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SelectType getType() {
        return type;
    }

    public void setType(SelectType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
