package devcamp.shop.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "province")
public class CProvince {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nhập tên tỉnh/thành phố")
    @Column(name = "_name", nullable = false)
    private String provinceName;

    @NotNull(message = "Nhập mã tỉnh/thành phố")
    @Size(min = 1, message = "Mã tỉnh/thành phố tối thiểu 1 ký tự")
    @Column(name = "_code", nullable = false)
    private String code;

    @OneToMany(mappedBy = "province", cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JsonIgnore
    private Set<CDistrict> districts;

    public CProvince() {
    }

    public CProvince(int id, @NotNull(message = "Nhập tên tỉnh/thành phố") String provinceName,
            @NotNull(message = "Nhập mã tỉnh/thành phố") @Size(min = 1, message = "Mã tỉnh/thành phố tối thiểu 1 ký tự") String code) {
        this.id = id;
        this.provinceName = provinceName;
        this.code = code;
        // this.districts = districts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    // public Set<CDistrict> getDistricts() {
    // return districts;
    // }

    // public void setDistricts(Set<CDistrict> districts) {
    // this.districts = districts;
    // }

}
