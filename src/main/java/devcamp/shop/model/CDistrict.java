package devcamp.shop.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "district")

public class CDistrict {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nhập tên quận/huyện")
    @Size(min = 1, message = "Tên quận/huyện tối thiểu 1 ký tự")
    @Column(name = "_name", nullable = false)
    private String districtName;

    @NotNull(message = "Nhập quận/huyện")
    @Column(name = "_prefix", nullable = false)
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "_province_id")
    private CProvince province;

    @OneToMany(mappedBy = "district", cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JsonIgnore
    private Set<CWard> wards = new HashSet<CWard>();

    @OneToMany(mappedBy = "district", cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JsonIgnore
    private Set<CStreet> streets = new HashSet<CStreet>();

    public CDistrict(int id,
            @NotNull(message = "Nhập tên quận/huyện") @Size(min = 1, message = "Tên quận/huyện tối thiểu 1 ký tự") String districtName,
            @NotNull(message = "Nhập quận/huyện") String prefix, CProvince province) {
        this.id = id;
        this.districtName = districtName;
        this.prefix = prefix;
        this.province = province;
    }

    public CDistrict() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

}
